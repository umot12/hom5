package com.example.bootdata.service.dtomapper;


import com.example.bootdata.domain.dto.EmployerRequestDto;

import com.example.bootdata.domain.hr.Employer;
import org.springframework.stereotype.Service;


@Service
public class EmployerRequestDtoMapper extends DtoMapperFacade<Employer, EmployerRequestDto>{
    public EmployerRequestDtoMapper (){super(Employer.class , EmployerRequestDto.class); }
}