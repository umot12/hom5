package com.example.bootdata.service.dtomapper;

import com.example.bootdata.domain.dto.AccountDto;

import com.example.bootdata.domain.dto.AccountRequestDto;
import com.example.bootdata.domain.hr.Account;
import org.springframework.stereotype.Service;


@Service
public class AccountRequestDtoMapper extends DtoMapperFacade<Account, AccountRequestDto>{
    public AccountRequestDtoMapper (){super(Account.class , AccountRequestDto.class); }
}