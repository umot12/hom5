package com.example.bootdata.service;

import com.example.bootdata.domain.hr.Account;

import java.util.List;
import java.util.UUID;

public interface  AccountService {
  List<Account > findAll(Integer page, Integer size);
    void save(Account account );
  void update(Account account );
    void delete(Account account );
    void deleteAll(List<Account> accountList );
    void saveAll (List <Account > accountList );
    List<Account > findAll();
  void deleteById(Long id);
    Account  getOne(long id);
  public boolean addMoney(UUID number, Double  amount );
  public boolean takeMoney(UUID number, Double  amount );

}
