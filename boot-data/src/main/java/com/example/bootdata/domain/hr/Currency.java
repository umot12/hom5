package com.example.bootdata.domain.hr;

public enum Currency {
    USD,
    EUR,
    UAH,
    CHF,
    GBP
}
