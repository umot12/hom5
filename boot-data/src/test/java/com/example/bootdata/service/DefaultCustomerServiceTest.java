package com.example.bootdata.service;

import com.example.bootdata.dao.CustomerJpaRepository;

import com.example.bootdata.domain.hr.Account;
import com.example.bootdata.domain.hr.Customer;
import com.example.bootdata.domain.hr.Employer;
import com.example.bootdata.service.DefaultCustomerService;
import com.example.bootdata.service.DefaultEmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DefaultCustomerServiceTest {

    @Mock
    private CustomerJpaRepository customerJpaRepository;


    @InjectMocks
    private DefaultCustomerService customerService;

    @Captor
    private ArgumentCaptor<Customer> customerArgumentCaptor;

    @Test
    public void testGetAllPageble() {
       Customer customer = new Customer();
        when(customerJpaRepository.findAll(any(Pageable.class)))
                .thenReturn(new PageImpl<>(List.of(customer)));
        List<Customer> customers = customerService.findAll(1, 2);

        assertEquals(customer, customers.get(0));
    }

    @Test
    public void test_GetAll_Success() {
      Customer customer1 = new Customer();
        Customer customer2 = new Customer();
        List<Customer> customersExpected = List.of(customer1, customer2);
        when(customerJpaRepository.findAll())
                .thenReturn(customersExpected);

        List<Customer> customersActual =customerService.findAll();
        assertNotNull(customersActual);
        assertFalse(customersActual.isEmpty());
        assertIterableEquals(customersExpected, customersActual);
    }

    @Test
    public void test_Create_Success() {
        Customer customer1 = new Customer();

        customerService.save(customer1);

        verify(customerJpaRepository).save(customerArgumentCaptor.capture());
        Customer customerActualArgument = customerArgumentCaptor.getValue();
        assertEquals(customer1, customerActualArgument);
    }
    @Test
    public void test_Put_Success() {
        Customer customer1 = new Customer() ;


        customer1.setName("Jane");
        customer1.setCreationDate(new Date());
        when(customerJpaRepository.getOne(customer1.getId())
        )
                .thenReturn(customer1);
        customerService.update(customer1);

        verify(customerJpaRepository).save(customerArgumentCaptor.capture());
      Customer   customerActualArgument = customerArgumentCaptor.getValue();
        assertEquals(customer1, customerActualArgument);
    }
    @Test
    public void test_Delete_Success() {
        Customer customer1= new Customer() ;

       customerService.save(customer1);
        customer1.setId(1L);

        customer1.setName("Jane");
        customerService.delete(customer1);

        verify(customerJpaRepository).delete(customerArgumentCaptor.capture());
        Customer  customerActualArgument = customerArgumentCaptor.getValue();
        assertEquals(customer1, customerActualArgument);
    }
    @Test
    public void test_GetById_Success() {
        Customer customer1 = new Customer();
        customer1.setId(1L);
        Customer customer2 = new Customer();
        customer2.setId(2L);
       Customer  customerExpected = customer2;
        when(customerJpaRepository.getOne(customer2.getId()))
                .thenReturn(customer2);

        Customer customerActual =customerService.getOne(customer2.getId());
        assertNotNull(customerActual);

        assertEquals(customerExpected, customerActual);
    }

}